import { NuxtAuthHandler } from "#auth";
import CredentialsProvider from 'next-auth/providers/credentials'

var auth = useRuntimeConfig().authentication
var auth_conf

if (auth === "LDAP") {
  auth_conf = {
    secret: useRuntimeConfig().secret,
    providers: [
      CredentialsProvider.default({
        name: 'Credentials',
  
        credentials: {
          username: { label: 'Username', type: 'text', placeholder: 'jsmith' },
          password: { label: 'Password', type: 'password' },
        },
        async authorize(credentials, req) {
          const token = Buffer.from(`${credentials.username}:${credentials.password}`).toString('base64')
          const res = await $fetch(useRuntimeConfig().public.openlinkApiURL+"/api/tokens/", {
            method: 'post',
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Basic ${token}`,
            },
          })
          if (res) {
            const u = {
              id: res.user.id,
              name: res.user.username,
              email: null,
              access_token: res.token
            }
            return u
          } else {
            return null
          }
        },
      }),
    ],
    session: {
      strategy: "jwt",
    },
    callbacks: {
      async session ({ session, token, user }) {
        session.token = token.openlinkToken;
        return session
      },
      async jwt ({token, user}) {
        const isSignIn = user ? true : false;
        if (isSignIn) {
          token.openlinkToken = user ? (user as any).access_token || '' : '';
        }
        return token;
      },
    },
  }
}
else if (auth === "OIDC") {
  auth_conf = {
    secret: useRuntimeConfig().secret,
    providers: [
      {
        id: "openid",
        name: "OpenID",
        wellKnown: useRuntimeConfig().oidcWellKnown,
        type: "oauth",
        authorization: { params: { scope: "openid email profile" } },
        //authorization: { params: { scope: "openid" } },
        checks: ["pkce", "state"],
        idToken: true,
        options: {
          clientId: useRuntimeConfig().oidcClientID,
          clientSecret: useRuntimeConfig().oidcClientSecret,
          issuer: useRuntimeConfig().oidcIssuer,
        },
        profile(profile) {
          var _profile$name;

          return {
            id: profile.sub,
            name:
              (_profile$name = profile.name) !== null && _profile$name !== void 0
                ? _profile$name
                : profile.preferred_username,
            email: profile.email,
            image: profile.picture,
          };
        },
      },
    ],
    callbacks: {
      async session ({ session, token, user }) {
          session.token = token.openlinkToken;
          return session
      },
      async jwt({ token, account, profile }) {
        // login to OpenLink API with JWT token
        if (account) {
          const data = await $fetch(useRuntimeConfig().public.openlinkApiURL+"/api/tokens/", {
            method: "POST",
            headers: {
              Authorization: "Token " + account.id_token,
            },
          });
          token.openlinkToken = data.token;
        }
        return token;
      },
    },
  }
}

export default NuxtAuthHandler(auth_conf)