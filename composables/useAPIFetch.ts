import { UseFetchOptions } from "#app"
import { Ref } from "vue"

export const useAPIFetch = (url: string | Request | Ref<string | Request> | (() => string | Request),  options?: UseFetchOptions<any>) => {
    const { status, data: session, signOut } = useSession()
    if(options == undefined) {
        options = {}
    }
    options.headers = options.headers || {}
    if(session.value != null) {
        options.headers.authorization = "Token " + session.value.token
    }
    options.baseURL = useRuntimeConfig().public.openlinkApiURL
    options.onResponseError = async ({ response }) => {
        if(response.status == 401) {
            signOut({ callbackUrl: '/'})
        }
    }
    return useFetch(url, options)
}
