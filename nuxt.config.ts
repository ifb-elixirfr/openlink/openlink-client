// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    runtimeConfig: {
        secret: "",
        oidcWellKnown: "",
        oidcIssuer: "",
        oidcClientID: "",
        oidcClientSecret: "",
        public: {
          openlinkApiURL: ""
        },
        authentication:""
    },
    css: [
        "vuetify/lib/styles/main.sass",
        "@mdi/font/css/materialdesignicons.min.css",
        "@/assets/css/openlink.css"
    ],
    build: {
        transpile: ["vuetify"],
    },
    vite: {
        define: {
        "process.env.DEBUG": false,
        },
    },
    modules: [
        "@sidebase/nuxt-auth",
        '@pinia/nuxt',
    ],
    auth: {
        enableGlobalAppMiddleware: true,
        origin: process.env.ORIGIN,
    },
    routeRules: {
        '/todo': { ssr: false},
        '/index': { ssr: false },
        '/investigations/*': { ssr: false},
        '/investigations/*/studies/*': { ssr: false},
        '/investigations/*/studies/*/assays/*': { ssr: false},
    }
});
