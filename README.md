# OpenLink client

## Development setup

Make sure to install the dependencies:

```bash
yarn install
```

Create your development `.env` config file :

```bash
ORIGIN=http://localhost:3000
NUXT_SECRET=a-secret-key
NUXT_OIDC_WELL_KNOWN=http://myoidchost/.well-known/openid-configuration
NUXT_OIDC_ISSUER=http://myoidchost/realm
NUXT_OIDC_CLIENT_ID=client-id
NUXT_OIDC_CLIENT_SECRET=client-secret
NUXT_PUBLIC_OPENLINK_API_URL=http://127.0.0.1:8000
```

Start the development server on http://localhost:3000

```bash
yarn dev
```
