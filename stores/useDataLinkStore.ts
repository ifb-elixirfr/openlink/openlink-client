import { defineStore } from 'pinia'
import { Ref } from 'vue'

interface Investigation {
    id: number|null,
    name: string|null,
    description: string|null,
    status: string,
    studies: Map<number, Study>
    opened: boolean
}

interface Study {
    id: number|null,
    name: string|null,
    description: string|null,
    status: string,
    assays: Map<number, Assay>
    opened: boolean
}

interface Assay {
    id: number|null,
    name: string|null,
    status: string,
    description: string|null
}

interface DataObject {
    id: string,
    name: string,
    type: string,
    icon: string,
    linkable: boolean,
    has_children: boolean,
    url: string
}

interface DataLink {
    id: number,
    name: string,
    dataobject_id: string,
    dataobject_type: string,
    tool_id: string
}

interface Tool {
    id: number,
    connector: string,
    name: string,
    parameters: {},
    async: boolean
}

export const useDataLinkStore = defineStore('datalink', () => {
    const currentISAId: Ref<null|number> = ref(null)
    const datalinks = ref(new Map<number, DataLink>)

    async function fetchDataLinks(ISAObject: Investigation | Study | Assay, ISAObjectType: string) {
        datalinks.value.clear()
        const { data: restDataLinks } = await useAPIFetch("/api/datalinks?parent_id="+ISAObject.id+"&parent_type="+ISAObjectType)
        restDataLinks.value.forEach(async restDataLink => {
            let datalink = {
                id: restDataLink.id,
                name: restDataLink.name,
                dataobject_id: restDataLink.dataobject_id,
                dataobject_type: restDataLink.dataobject_type,
                tool_id: restDataLink.tool_id
            } as DataLink
            datalinks.value.set(datalink.id, datalink)
        })
    }

    async function createDataLink(ISAObject: Investigation | Study | Assay, ISAObjectType: string, tool: Tool, object: DataObject) {
        let body = {
            parent_id: ISAObject.id,
            parent_type: ISAObjectType,
            tool: tool.id,
            data_object_id: object.id,
            data_object_type: object.type
        }
        const { data: restDataLink } = await useAPIFetch('/api/datalinks/', {
            method: 'POST',
            body: body
        })

        let datalink = {
            id: restDataLink.value.id,
            name: restDataLink.value.name,
            dataobject_id: restDataLink.value.dataobject_id,
            dataobject_type: restDataLink.value.dataobject_type,
            tool_id: restDataLink.value.tool_id
        } as DataLink

        datalinks.value.set(datalink.id, datalink)
    }

    return {
        currentISAId,
        datalinks,
        fetchDataLinks,
        createDataLink
    }
})
