import { defineStore } from 'pinia'
import { Ref } from 'vue'

interface Connector {
    id: string,
    name: string,
    icon: string,
    description: string
    fields: ConnectorField[]
}

interface ConnectorField {
    id: string,
    name: string,
    helper: string,
    type: string,
    access: string,
    value: any
}

interface Tool {
    id: number,
    connector: string,
    name: string,
    parameters: {},
    async: boolean
}

interface DataObject {
    id: string,
    name: string,
    type: string,
    icon: string,
    linkable: boolean,
    has_children: boolean,
    url: string
    children: DataObject[]
}

interface ChildrenObject {
    parent: string,
    name: string,
    type: string,
}


export const useToolStore = defineStore('tool', () => {
    const currentISAId: Ref<null|number> = ref(null)
    const dataobjects = ref(new Map<number, DataObject>)
    const connectors = ref(new Map<string, Connector>)
    const tools = ref(new Map<number, Tool>)
    const children_objects = [] as ChildrenObject[]

    async function fetchConnectors(connector_type: string) {
        connectors.value.clear()
        if(connector_type == undefined) {
            connector_type = ""
        }
        const { data: restConnectors } = await useAPIFetch("/api/connectors?type="+connector_type)
        // add/update available connectors
        
        restConnectors.value.forEach(restConnector => {
            let connector = {
                id: restConnector.id,
                name: restConnector.name,
                description: restConnector.description,
                icon: restConnector.icon,
                fields: [] as ConnectorField[]
            } as Connector

            restConnector.fields.forEach(restField => {
                connector.fields.push({
                    id: restField.id,
                    name: restField.name,
                    helper: restField.helper,
                    type: restField.type,
                    access: restField.access,
                    value: null
                } as ConnectorField)
            })

            connectors.value.set(connector.id, connector)
            
        })
    }

    async function fetchTools(connector_type: string) {
        tools.value.clear()
        if(connector_type == undefined) {
            connector_type = ""
        }
        const { data: restTools } = await useAPIFetch("/api/tools?type="+connector_type)
        restTools.value.forEach(restTool =>  {
            let tool = {
                id: restTool.id,
                connector: restTool.connector,
                name: restTool.name,
                parameters: {},
                async: false} as Tool
                //async: restTool.async
            //Object.entries(restTool.parameters).forEach(([key, value]) => {
            //    tool.parameters[key] = value
            //})
            tools.value.set(tool.id, tool)
        })
    }

    async function createTool(connector: Connector) {
        let body = {
            connector: connector.id,
            parameters: Object.assign({}, ...connector.fields.map((field) => ({[field.id]: field.value}))),
            async: false,
            name: null
        }
        const { data: restTool } = await useAPIFetch('/api/tools/', {
            method: 'POST',
            body: body
        })
        let tool = {id: restTool.value.id, connector: restTool.value.connector, name: restTool.value.name, parameters: {}, async: restTool.value.async} as Tool
        Object.entries(restTool.value.parameters).forEach(([key, value]) => {
            tool.parameters[key] = value
        })
        tools.value.set(tool.id, tool)
        return tool
    }

    async function fetchDataObjects(tool: Tool, parent?: DataObject) {
        let url = '/api/tools/' + tool.id + '/objects/'
        if(parent) {
            url = url + '?parent_id=' + parent.id + '&parent_type=' + parent.type
        }
        const { data: restObjects } = await useAPIFetch(url)
        let objects = [] as DataObject[]
        restObjects.value.objects.forEach(restObject => {
            objects.push({
                id: restObject.id,
                name: restObject.name,
                type: restObject.type,
                icon: restObject.icon,
                linkable: restObject.linkable,
                has_children: restObject.has_children,
                url: restObject.url,
                children: restObject.children,
            } as DataObject)
        })
        return { root_name: restObjects.value.root_name, objects: objects}
    }


    return {
        currentISAId,
        connectors,
        tools,
        fetchConnectors,
        fetchTools,
        createTool,
        fetchDataObjects,
    }
})
