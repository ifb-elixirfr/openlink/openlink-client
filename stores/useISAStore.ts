import { defineStore } from 'pinia'
import { Ref } from "vue"

interface Investigation {
    id: number|null,
    name: string|null,
    description: string|null,
    status: string,
    studies: Map<number, Study>
    opened: boolean
}

interface Study {
    id: number|null,
    name: string|null,
    description: string|null,
    status: string,
    assays: Map<number, Assay>
    opened: boolean
}

interface Assay {
    id: number|null,
    name: string|null,
    status: string,
    description: string|null
}

interface StudySelector {
    iid: number,
    sid: number
}

interface AssaySelector {
    iid: number,
    sid: number,
    aid: number
}

interface Tool {
    id: number,
    connector: string,
    name: string,
    parameters: {},
    async: boolean
}

interface DataObject {
    id: string,
    name: string,
    type: string,
    icon: string,
    linkable: boolean,
    has_children: boolean,
    url: string,
    children: DataObject[]
}

interface DataLink {
    id: number,
    name: string,
    dataobject_id: string,
    dataobject_type: string,
    tool_id: string
}

export const useISAStore = defineStore('isa', () => {
    //states
    const investigations = ref(new Map<number, Investigation>)

    const currentIid:Ref<null|number> = ref(null)
    const currentSid:Ref<null|StudySelector> = ref(null)
    const currentAid:Ref<null|AssaySelector> = ref(null)

    const updatedInvestigations = ref([] as number[])
    const updatedStudies = ref([] as StudySelector[])
    const updatedAssays = ref([] as AssaySelector[])

    const datalinks = ref(new Map<number, DataLink>)

    // getters
    const currentInvestigation = computed(() => {
        if(currentIid.value != null && investigations.value.has(currentIid.value)) {
            return investigations.value.get(currentIid.value)
        }
        return {id: null, name: null, description: null, status: 'pending', studies: new Map<number, Study>, opened: false} as Investigation
    })
    const currentStudy = computed(() => {
        if(currentSid.value != null && investigations.value.has(currentSid.value.iid) && investigations.value.get(currentSid.value.iid)?.studies.has(currentSid.value.sid)) {
            return investigations.value.get(currentSid.value.iid)?.studies.get(currentSid.value.sid)
        }
        return {id: null, name: null, description: null, status: 'pending', assays: new Map<number, Assay>, opened: false} as Study
    })
    const currentAssay = computed(() => {
        if(currentAid.value != null && investigations.value.has(currentAid.value.iid) && investigations.value.get(currentAid.value.iid)?.studies.has(currentAid.value.sid) && investigations.value.get(currentAid.value.iid)?.studies.get(currentAid.value.sid)?.assays.has(currentAid.value.aid)) {
            return investigations.value.get(currentAid.value.iid).studies.get(currentAid.value.sid).assays.get(currentAid.value.aid)
        }
        return {id: null, name: null, description: null, status: 'pending', assays: new Map<number>} as Assay
    })
    const investigationsCount = computed(() => investigations.value.size)

    const hasPendingModification = computed(() => updatedInvestigations.value.length + updatedStudies.value.length + updatedAssays.value.length > 0)

    // actions
    async function createPendingInvestigation(name: string|null) {
        const { data } = await useAPIFetch('/api/investigations/', {
            method: 'POST',
            body: {name: name, status: 'pending'}
        })
        let investigation = {id: data.value.id, name: data.value.name, description: data.value.description, status: data.value.status, studies: new Map<number, Study>, opened: false} as Investigation
        investigations.value.set(data.value.id, investigation)
        return investigation
    }

    async function createPendingStudy(parentIId: number) {
        const { data } = await useAPIFetch('/api/investigations/'+parentIId+'/studies/', {
            method: 'POST',
            body: {status: 'pending'}
        })
        let study = {id: data.value.id, name: data.value.name, description: data.value.description, status: data.value.status, assays: new Map<number, Assay>, opened: false} as Study
        if(investigations.value.has(parentIId)) {
            investigations.value.get(parentIId)?.studies.set(data.value.id, study)
        }
        return study
    }

    async function createPendingAssay(parentIId: number, parentSId: number) {
        const { data } = await useAPIFetch('/api/studies/'+parentSId+'/assays/', {
            method: 'POST',
            body: {status: 'pending'}
        })
        let assay = {id: data.value.id, name: data.value.name, description: data.value.description, status: data.value.status} as Assay
        if(investigations.value.has(parentIId) && investigations.value.get(parentIId)?.studies.has(parentSId)) {
            investigations.value.get(parentIId)?.studies.get(parentSId)?.assays.set(data.value.id, assay)
        }
        return assay
    }

    async function fetchInvestigations() {
        const { data: restInvestigations } = await useAPIFetch("/api/investigations/")
        let currentIids = [] as number[]

        // add missing investigations
        restInvestigations.value.forEach(investigation => {
            if(! investigations.value.has(investigation.id)) {
                investigations.value.set(investigation.id, {id: investigation.id, name: investigation.name, description: investigation.description, status: investigation.status, studies: new Map<number, Study>, opened: false} as Investigation)
            }
            currentIids.push(investigation.id)
        })

        investigations.value.forEach(async (investigation: Investigation, key: number) => {
            if(key != 0 && ! currentIids.includes(key)) {
                // remove investigations deleted remotely
                investigations.value.delete(key)
            } else {
                if(investigation.opened) {
                    // fetch studies of opened investigations
                    await fetchStudies(key)
                }
            }
        });
    }
    async function fetchStudies(iid: number) {
        // add missing studies
        const { data: restStudies } = await useAPIFetch('api/investigations/'+iid+'/studies/')
        let currentSids = [] as number[]
        restStudies.value.forEach(study => {
            if(! investigations.value.get(iid)?.studies.has(study.id)) {
                investigations.value.get(iid)?.studies.set(study.id, {id: study.id, name: study.name, description: study.description, status: study.status, assays: new Map<number, Assay>, opened: false} as Study)
            }
            currentSids.push(study.id)
        })
        // remove deleted studies
        investigations.value.get(iid)?.studies.forEach(async (study: Study, key: number) => {
            if(key != 0 && ! currentSids.includes(key)) {
                investigations.value.get(iid)?.studies.delete(key)
            } else {
                if(study.opened) {
                    await fetchAssays(iid, key)
                }
            }
        })
    }
    async function fetchAssays(iid:number, sid: number) {
        // add missing assays
        const { data: restAssays } = await useAPIFetch('api/studies/'+sid+'/assays/')
        let currentAids = [] as number[]
        restAssays.value.forEach(assay => {
            if(! investigations.value.get(iid)?.studies.get(sid)?.assays.has(assay.id)) {
                investigations.value.get(iid)?.studies.get(sid)?.assays.set(assay.id, {id: assay.id, name: assay.name, description: assay.description, status: assay.status } as Assay)
            }
            currentAids.push(assay.id)
        })
        // remove deleted studies
        investigations.value.get(iid)?.studies.get(sid)?.assays.forEach((value: Assay, key: number) => {
            if(key != 0 && ! currentAids.includes(key)) {
                investigations.value.get(iid)?.studies.get(sid)?.assays.delete(key)
            }
        })
    }
    async function setCurrentInvestigation(iid: number) {
        if(investigations.value.has(iid)) {
            currentIid.value = iid
            currentSid.value = null
            currentAid.value = null
            return true
        }
        return false
    }
    async function setCurrentStudy(iid: number, sid: number) {
        if(investigations.value.has(iid) && investigations.value.get(iid)?.studies.has(sid)) {
            currentIid.value = null
            currentSid.value = {iid: iid, sid: sid} as StudySelector
            currentAid.value = null
            investigations.value.get(iid).opened = true

            return true
        }
        return false
    }
    async function setCurrentAssay(iid: number, sid: number, aid: number) {
        if(investigations.value.has(iid) && investigations.value.get(iid).studies.has(sid) && investigations.value.get(iid).studies.get(sid)?.assays.has(aid)) {
            currentIid.value = null
            currentSid.value = null
            currentAid.value = {iid: iid, sid: sid, aid: aid} as AssaySelector
            investigations.value.get(iid).opened = true
            investigations.value.get(iid).studies.get(sid).opened = true

            return true
        }
        return false
    }
    async function deleteCurrentInvestigation() {
        if(currentIid.value != null) {
            await useAPIFetch('/api/investigations/'+currentIid.value+'/', {
                method: 'DELETE'
            })
            investigations.value.delete(currentIid.value)
            currentIid.value = null
        }
    }
    async function deleteCurrentStudy() {
        if(currentSid.value != null) {
            await useAPIFetch('/api/studies/'+currentSid.value.sid+'/', {
                method: 'DELETE'
            })
            investigations.value.get(currentSid.value.iid).studies.delete(currentSid.value.sid)
            currentSid.value = null
        }
    }
    async function deleteCurrentAssay() {
        if(currentAid.value != null) {
            await useAPIFetch('/api/assays/'+currentAid.value.aid+'/', {
                method: 'DELETE'
            })
            investigations.value.get(currentAid.value.iid).studies.get(currentAid.value.sid)?.assays.delete(currentAid.value.aid)
            currentAid.value = null
        }
    }
    function addUpdatedInvestigation(iid: number) {
        if(updatedInvestigations.value.indexOf(iid) == -1) {
            updatedInvestigations.value.push(iid)
        }
    }
    function addUpdatedStudy(iid: number, sid: number) {
        if(updatedStudies.value.map(s => s.sid).indexOf(sid) == -1) {
            updatedStudies.value.push({iid: iid, sid: sid} as StudySelector)
        }
    }
    function addUpdatedAssay(iid: number, sid: number, aid: number) {
        if(updatedAssays.value.map(s => s.aid).indexOf(aid) == -1) {
            updatedAssays.value.push({iid: iid, sid: sid, aid: aid} as AssaySelector)
        }
    }

    async function saveAndRefresh() {
        updatedInvestigations.value.forEach(async (iid) => {
            if(investigations.value.has(iid)) {
                await useAPIFetch('/api/investigations/'+iid+'/', {
                    method: 'PUT',
                    body: {name: investigations.value.get(iid)?.name, description: investigations.value.get(iid)?.description, status: investigations.value.get(iid)?.status}
                })
            }

            const index = updatedInvestigations.value.indexOf(iid, 0)
            if(index > -1) {
                updatedInvestigations.value.splice(index, 1)
            }
        })
        updatedStudies.value.forEach(async (updatedStudy) => {
            if(investigations.value.has(updatedStudy.iid) && investigations.value.get(updatedStudy.iid)?.studies.has(updatedStudy.sid)) {
                const study = investigations.value.get(updatedStudy.iid)?.studies.get(updatedStudy.sid)
                await useAPIFetch('/api/studies/'+updatedStudy.sid+'/', {
                    method: 'PUT',
                    body: {name: study?.name, description: study?.description, status: study?.status}
                })

                const index = updatedStudies.value.indexOf(updatedStudy, 0)
                if(index > -1) {
                    updatedStudies.value.splice(index, 1)
                }
            }
        })
        updatedAssays.value.forEach(async (updatedAssay) => {
            if(investigations.value.has(updatedAssay.iid) && investigations.value.get(updatedAssay.iid)?.studies.has(updatedAssay.sid) && investigations.value.get(updatedAssay.iid)?.studies.get(updatedAssay.sid)?.assays.has(updatedAssay.aid)) {
                const assay = investigations.value.get(updatedAssay.iid)?.studies.get(updatedAssay.sid)?.assays.get(updatedAssay.aid)
                await useAPIFetch('/api/assays/'+updatedAssay.aid+'/', {
                    method: 'PUT',
                    body: {name: assay?.name, description: assay?.description, status: assay?.status}
                })

                const index = updatedAssays.value.indexOf(updatedAssay, 0)
                if(index > -1) {
                    updatedAssays.value.splice(index, 1)
                }
            }
        })

        await fetchInvestigations()
    }

    async function updateInvestigationByImport(tool: Tool, dataobject: DataObject, ISAObject: Investigation, dry_run: boolean) {
        const { data: importISAObject  } = await useAPIFetch('/api/investigations/' + ISAObject.id + '/?import_from={"tool_id": "' + tool.id + '", "dataobject_id": "' + dataobject.id + '", "dataobject_type": "' + dataobject.type +'"}&dry_run=' + dry_run, {
            method: 'PUT',
            body: {name: ISAObject?.name, description: ISAObject?.description, status: ISAObject?.status}
        })
        if(dry_run==false) {
            let investigation = {id: importISAObject.value.id, name: importISAObject.value.name, description: importISAObject.value.description, status: importISAObject.value.status, studies: new Map<number, Study>, opened: true} as Investigation
            importISAObject.value.datalinks.forEach((restDataLink) => {
                let datalink = {
                    id: restDataLink.id,
                    name: restDataLink.name,
                    dataobject_id: restDataLink.dataobject_id,
                    dataobject_type: restDataLink.dataobject_type,
                    tool_id: restDataLink.tool_id
                } as DataLink
                datalinks.value.set(restDataLink.id, datalink)
            })
            importISAObject.value.children.forEach((restStudy) => {
                let study = {
                    id: restStudy.id,
                    name: restStudy.name,
                    description: restStudy.description,
                    status: "active",
                    assays: new Map<number, Assay>
                } as Study
                investigation.studies.set(restStudy.id, study)
            })
            investigations.value.set(importISAObject.value.id, investigation)
        }
        return importISAObject.value
    }

    async function updateStudyByImport(tool: Tool, dataobject: DataObject, InvestigationId: number, ISAObject: Study, dry_run: boolean) {
        const { data: importISAObject  } = await useAPIFetch('/api/studies/' + ISAObject.id + '/?import_from={"tool_id": "' + tool.id + '", "dataobject_id": "' + dataobject.id + '", "dataobject_type": "' + dataobject.type +'"}&dry_run=' + dry_run, {
            method: 'PUT',
            body: {name: ISAObject?.name, description: ISAObject?.description, status: ISAObject?.status}
        })
        if(dry_run==false) {
            let study = {id: importISAObject.value.id, name: importISAObject.value.name, description: importISAObject.value.description, status: importISAObject.value.status, assays: new Map<number, Assay>, opened: true} as Study
            importISAObject.value.datalinks.forEach((restDataLink) => {
                let datalink = {
                    id: restDataLink.id,
                    name: restDataLink.name,
                    dataobject_id: restDataLink.dataobject_id,
                    dataobject_type: restDataLink.dataobject_type,
                    tool_id: restDataLink.tool_id
                } as DataLink
                datalinks.value.set(restDataLink.id, datalink)
            })
            importISAObject.value.children.forEach((restAssay) => {
                let assay = {
                    id: restAssay.id,
                    name: restAssay.name,
                    description: restAssay.description,
                    status: "active"
                    } as Assay
                study.assays.set(restAssay.id, assay)
            })
            investigations.value.get(InvestigationId)?.studies.set(importISAObject.value.id, study)
        }
        return importISAObject.value
    }

    async function updateAssayByImport(tool: Tool, dataobject: DataObject, InvestigationId: number, StudyId: number, ISAObject: Assay, dry_run: boolean) {
        const { data: importISAObject  } = await useAPIFetch('/api/assays/' + ISAObject.id + '/?import_from={"tool_id": "' + tool.id + '", "dataobject_id": "' + dataobject.id + '", "dataobject_type": "' + dataobject.type +'"}&dry_run=' + dry_run, {
            method: 'PUT',
            body: {name: ISAObject?.name, description: ISAObject?.description, status: ISAObject?.status}
        })
        if(dry_run==false) {
            let assay = {id: importISAObject.value.id, name: importISAObject.value.name, description: importISAObject.value.description, status: importISAObject.value.status, opened: true} as Assay
            importISAObject.value.datalinks.forEach((restDataLink) => {
                let datalink = {
                    id: restDataLink.id,
                    name: restDataLink.name,
                    dataobject_id: restDataLink.dataobject_id,
                    dataobject_type: restDataLink.dataobject_type,
                    tool_id: restDataLink.tool_id
                } as DataLink
                datalinks.value.set(restDataLink.id, datalink)
            })
            investigations.value.get(InvestigationId)?.studies.get(StudyId)?.assays.set(importISAObject.value.id, assay)
        }
        return importISAObject.value      
    }

    return {
        //states
        investigations,
        currentIid,
        currentSid,
        currentAid,
        updatedInvestigations,
        updatedStudies,
        updatedAssays,
        //getters
        currentInvestigation,
        currentStudy,
        currentAssay,
        investigationsCount,
        hasPendingModification,
        //actions
        createPendingInvestigation,
        createPendingStudy,
        createPendingAssay,
        fetchInvestigations,
        fetchStudies,
        fetchAssays,
        setCurrentInvestigation,
        setCurrentStudy,
        setCurrentAssay,
        deleteCurrentInvestigation,
        deleteCurrentStudy,
        deleteCurrentAssay,
        addUpdatedInvestigation,
        addUpdatedStudy,
        addUpdatedAssay,
        saveAndRefresh,
        updateInvestigationByImport,
        updateStudyByImport,
        updateAssayByImport
    }
})

